from db_cleanup import truncate_old


def main():
    truncate_old()


if __name__ == "__main__":
    main()
