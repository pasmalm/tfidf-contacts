import pymysql
import json
import re
import pandas as pd
import get_documents as gd
import snowballstemmer
from configreader import getconfig
from sklearn.feature_extraction.text import TfidfVectorizer

pymysql.install_as_MySQLdb()


def gather_documents(sev_client, db_connection):
    """
    use get_documents to get all documents for certain sev client
    as a list of strings, 1 string for each contact of that sev_client who
    has documents
    :param db_connection: connection object to octopus db
    :param sev_client: id of sev_client
    :return: list<str> list with 1 string for each contact that has documents
    referenced
    """
    # config = configparser.ConfigParser()
    #
    # s3 = boto3.resource("s3")
    # ob = s3.Object("doc-to-contact-artifacts", "config.ini")
    #
    # config.read(ob.get()["Body"].read().decode("utf-8"))
    config = getconfig()

    contact_doc = gd.get_contacts_docs(sev_client, db_connection)

    disdar_host = config["source_db"]["disdar_host"]
    disdar_user = config["source_db"]["disdar_user"]
    disdar_pw = config["source_db"]["disdar_pw"]
    disdar_db = config["source_db"]["disdar_db"]

    conn2 = pymysql.connect(host=disdar_host,
                            user=disdar_user,
                            password=disdar_pw,
                            database=disdar_db)

    contact_uuids = {}

    for contact in contact_doc:
        if contact not in contact_uuids:
            contact_uuids[contact] = []
        for docid in contact_doc[contact]:
            uuid = gd.get_uuid_of_docid(docid, conn2)
            if uuid is not None:
                contact_uuids[contact].append(uuid)

    del contact_doc  # no longer needed
    conn2.close()

    contacts_document = {}

    for contact in contact_uuids:
        templist = []
        for uuid in contact_uuids[contact]:
            templist.append(gd.parse_ocr(gd.get_ocrjson(uuid)))
        contacts_document[contact] = " ".join([item for sublist in templist for
                                               item in sublist])
    del contact_uuids  # noo longer needed
    return contacts_document


def gather_documents_debug(sev_client, db_connection):
    contacts_sql = """SELECT 
    supplier, result_disdar
FROM
    stitch_sev_db.voucher
WHERE
    result_disdar IS NOT NULL
        AND supplier IS NOT NULL
        AND document IS NOT NULL
        AND status >= 100
        AND credit_debit = 'C'
        AND sev_client = {}
;"""
    # config = configparser.ConfigParser()

    # p = os.path.realpath(__file__)
    # confpath = os.path.join(os.path.dirname(p), "config.ini")
    # config.read(confpath)

    contactdocs = {}

    cursor = db_connection.cursor()
    n = cursor.execute(contacts_sql.format(sev_client))
    while True:
        record = cursor.fetchone()
        if record is None:
            break
        try:
            disdar = json.loads(record[1], encoding="utf-8")
        except json.JSONDecodeError:
            continue
        try:
            words = disdar.get("words", [])
        except AttributeError:  # words object has no get method -> not a
            # dictionary
            continue
        if not words:
            continue
        supplier = int(record[0])
        if supplier not in contactdocs:
            contactdocs[supplier] = []
        contactdocs[supplier] += words
    cursor.close()

    out = {}
    for contact in contactdocs:
        out[contact] = " ".join(contactdocs[contact])
    return out


def gather_all_documents(sev_clients, db_con):
    query = """SELECT sev_client, supplier, result_disdar
FROM
    stitch_sevdesk.voucher
WHERE
    result_disdar IS NOT NULL
        AND supplier IS NOT NULL
        AND document IS NOT NULL
        AND status >= 100
        AND credit_debit = 'C'
        AND sev_client in {}
;"""
    cursor = db_con.cursor()

    cursor.execute(query.format(tuple(sev_clients)))
    df = pd.DataFrame(columns=["sev_client", "contact", "result_disdar"],
                      data=cursor.fetchall())

    cursor.close()

    return df


def tokenize_and_stem(text):
    stemmer = snowballstemmer.stemmer("german")
    tokens = re.split(r"[^a-zA-Z0-9äöüÄÖÜß]", text)
    filtered_tokens = []
    # filter out any tokens not containing letters (e.g., numeric tokens, raw
    # punctuation)

    for token in tokens:
        if len(token) < 2:
            continue
        else:
            filtered_tokens.append(token)

    stems = [stemmer.stemWord(t) for t in filtered_tokens]
    return stems


def vectorization(doclist):
    """
    take list of strings as documents and create Docterm Matrix, Vocabulary
    and IDF-Vector
    :param doclist: list of documents (string with all words)
    :return: tuple with Docterm Matrix, vocabulary and IDF-Vector
    """

    ll = len(doclist)

    maxdf = ll-2 if ll > 10 else 1.0
    mindf = 2 if ll > 10 else 1
    vecto = TfidfVectorizer(doclist,
                            min_df=mindf,
                            max_df=maxdf,
                            tokenizer=tokenize_and_stem
                            )
    dtm = vecto.fit_transform(doclist)
    # print("doctermmatrix: ", dtm.shape)

    return dtm, vecto.vocabulary_, vecto.idf_
