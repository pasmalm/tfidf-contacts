import datetime
import pymysql
import warnings
import os
import json
import user_docvecs as udv
import psycopg2
from configreader import getconfig
from dateutil.relativedelta import relativedelta
pymysql.install_as_MySQLdb()


def write_to_db(sev_client, db_connection, debug=False):
    insert_vocabulary = '''INSERT IGNORE INTO words (word) VALUES (%s);'''

    insert_tf = """INSERT INTO Term_Frequency (contact, word, count, 
    sev_client) VALUES ((%s), (%s), (%s), (%s));"""

    insert_idf = """INSERT INTO InverseDocumentFrequency (idf, sev_client, 
    word, index_word) VALUES ((%s), (%s), (%s), (%s));"""

    get_wordids = """select word, id_word from words where `word` in {};"""

    # config = configparser.ConfigParser()
    #
    # p = os.path.realpath(__file__)
    # confpath = os.path.join(os.path.dirname(p), "config.ini")
    # config.read(confpath)

    config = getconfig()

    idf_host = config["tfidf_db"]["tfidf_host"]
    idf_user = config["tfidf_db"]["tfidf_user"]
    idf_pw = config["tfidf_db"]["tfidf_pw"]
    idf_db = config["tfidf_db"]["tfidf_db"]

    if debug:
        cont_docs = udv.gather_documents_debug(sev_client, db_connection)
    else:
        cont_docs = udv.gather_documents(sev_client, db_connection)
    if len(cont_docs) < 1:
        return
    contacts = list(cont_docs.keys())
    docs = list(cont_docs.values())

    doctermmatrix, vocabulary, idf = udv.vectorization(docs)
    idf = idf.tolist()

    vocabulary = {x: int(y) for x, y in vocabulary.items()}
    decabulary = {y: x for x, y in vocabulary.items()}

    conn = pymysql.connect(host=idf_host,
                           user=idf_user,
                           password=idf_pw,
                           database=idf_db)

    cursor = conn.cursor()

    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        cursor.executemany(insert_vocabulary, [(i,) for i in vocabulary])
    conn.commit()
    print("inserted Words..")
    words = tuple(vocabulary.keys())
    cursor.execute(get_wordids.format(words))

    word_ids = dict(cursor.fetchall())

    cursor.executemany(insert_idf,
                       [
                           (
                               idf[vocabulary[word]],
                               sev_client,
                               word_ids[word],
                               vocabulary[word]
                           )
                           for word in vocabulary
                       ])

    conn.commit()
    print("inserted idf")
    cursor.executemany(insert_tf,
                       [
                           (
                               contacts[index[0]],
                               word_ids[decabulary[index[1]]],
                               float(doctermmatrix[index]),
                               sev_client
                           )
                           for index in zip(*doctermmatrix.nonzero())
                       ])

    conn.commit()
    print("inserted termfrequencies")

    cursor.close()
    conn.close()


def write_to_sqlfile(sev_client, instance, db_connection, debug=False):
    insert_vocabulary = '''INSERT IGNORE INTO words (word) VALUES ('{}');'''

    insert_tf = """INSERT INTO Term_Frequency (contact, word, count, """ \
        """sev_client, instance) VALUES """
    tf_values = """({}, (select id_word from words where word = '{}'), """ \
                """{}, {}, '{}'),"""

    insert_idf = """INSERT INTO InverseDocumentFrequency (idf, sev_client, """ \
        """word, index_word, instance) VALUES """
    idf_values = """({}, {}, (select id_word from words where word = '{}'),""" \
                 """ {}, '{}'),"""

    # config = configparser.ConfigParser()
    #
    # p = os.path.realpath(__file__)
    # confpath = os.path.join(os.path.dirname(p), "config.ini")
    # config.read(confpath)
    config = getconfig()

    datapath = config["sql_file"]["sqlfile_path"]

    if debug:
        cont_docs = udv.gather_documents_debug(sev_client, db_connection)
    else:
        cont_docs = udv.gather_documents(sev_client, db_connection)
    if len(cont_docs) < 1:
        return
    contacts = list(cont_docs.keys())
    docs = list(cont_docs.values())

    doctermmatrix, vocabulary, idf = udv.vectorization(docs)
    idf = idf.tolist()

    vocabulary = {x: int(y) for x, y in vocabulary.items()}
    decabulary = {y: x for x, y in vocabulary.items()}

    with open(os.path.join(datapath, instance+config["sql_file"][
                "sqlfile_words"]), 'a') as words:
        for word in vocabulary:
            print(insert_vocabulary.format(word),
                  file=words)

    with open(os.path.join(datapath, instance+config["sql_file"]["sqlfile_tf"]),
              'a') as tf:
        print("SET autocommit=0;", file=tf)
        c = 0
        insert_counter = 1
        insert_query = insert_tf
        for index in zip(*doctermmatrix.nonzero()):
            insert_query += tf_values.format(contacts[index[0]],
                                             decabulary[index[1]],
                                             doctermmatrix[index],
                                             sev_client,
                                             instance)
            c += 1
            if c >= 100:
                print(insert_query[:-1]+";", file=tf)
                insert_counter += 1
                c = 0
                insert_query = insert_tf
            if insert_counter % 100 == 0:
                print("COMMIT;", file=tf)
                insert_counter = 1
        if c > 0:
            print(insert_query[:-1] + ";", file=tf)
        print("COMMIT;\nSET autocommit=1;", file=tf)

    with open(os.path.join(datapath, instance+config["sql_file"][
                "sqlfile_idf"]), 'a') as idffile:
        print("SET autocommit=0;", file=idffile)
        c = 0
        insert_counter = 1
        idf_insert = insert_idf
        for word in vocabulary:
            idf_insert += idf_values.format(idf[vocabulary[word]],
                                            sev_client,
                                            word,
                                            vocabulary[word],
                                            instance)
            c += 1
            if c >= 100:
                print(idf_insert[:-1] + ";", file=idffile)
                insert_counter += 1
                c = 0
                idf_insert = insert_idf
            if insert_counter % 100 == 0:
                print("COMMIT;", file=idffile)
                insert_counter = 1
        if c > 0:
            print(idf_insert[:-1] + ";", file=idffile)
        print("COMMIT;\nSET autocommit=1;", file=idffile)


def write_df_tosql(dataframe, instance):
    dataframe["words"] = dataframe["result_disdar"].apply(load_disdar_words)

    insert_vocabulary = '''INSERT IGNORE INTO words (word) VALUES ('{}');'''

    insert_tf = """INSERT INTO Term_Frequency (contact, word, count, 
        sev_client, instance)
            VALUES """
    tf_values = """({}, (select id_word from words where word = '{}'), """ \
                """{}, {}, '{}'),"""

    insert_idf = """INSERT INTO InverseDocumentFrequency (idf, sev_client, """ \
                 """word, index_word, instance) VALUES """
    idf_values = """({}, {}, (select id_word from words where word = '{}'),""" \
                 """ {}, '{}'),"""

    # config = configparser.ConfigParser()
    #
    # p = os.path.realpath(__file__)
    # confpath = os.path.join(os.path.dirname(p), "config.ini")
    # config.read(confpath)
    config = getconfig()

    datapath = config["sql_file"]["sqlfile_path"]

    for client in dataframe.sev_client.unique():
        documents = dataframe.loc[dataframe.sev_client == client].groupby(
            "contact")["words"].apply(" ".join)

        contacts = list(documents.keys())
        dox = list(documents.values)
        if len(dox) == 0 or not any(dox):
            continue
        try:
            doctermmatrix, vocabulary, idf = udv.vectorization(dox)
        except ValueError:
            continue
        idf = idf.tolist()
        vocabulary = {x: int(y) for x, y in vocabulary.items()}
        decabulary = {y: x for x, y in vocabulary.items()}

        with open(os.path.join(datapath, instance + config["sql_file"][
                "sqlfile_words"]), 'a') as words:
            for word in vocabulary:
                print(insert_vocabulary.format(word),
                      file=words)

        with open(os.path.join(datapath,
                               instance + config["sql_file"]["sqlfile_tf"]),
                  'a') as tf:
            print("SET autocommit=0;", file=tf)
            c = 0
            insert_counter = 1
            insert_query = insert_tf
            for index in zip(*doctermmatrix.nonzero()):
                insert_query += tf_values.format(contacts[index[0]],
                                                 decabulary[index[1]],
                                                 doctermmatrix[index],
                                                 client,
                                                 instance)
                c += 1
                if c >= 100:
                    print(insert_query[:-1] + ";", file=tf)
                    insert_counter += 1
                    c = 0
                    insert_query = insert_tf
                if insert_counter % 100 == 0:
                    print("COMMIT;", file=tf)
                    insert_counter = 1
            if c > 0:
                print(insert_query[:-1] + ";", file=tf)
            print("COMMIT;\nSET autocommit=1;", file=tf)

        with open(os.path.join(datapath, instance + config["sql_file"][
                "sqlfile_idf"]), 'a') as idffile:
            print("SET autocommit=0;", file=idffile)
            c = 0
            insert_counter = 1
            idf_insert = insert_idf
            for word in vocabulary:
                idf_insert += idf_values.format(idf[vocabulary[word]],
                                                client,
                                                word,
                                                vocabulary[word],
                                                instance)
                c += 1
                if c >= 100:
                    print(idf_insert[:-1] + ";", file=idffile)
                    insert_counter += 1
                    c = 0
                    idf_insert = insert_idf
                if insert_counter % 100 == 0:
                    print("COMMIT;", file=idffile)
                    insert_counter = 1
            if c > 0:
                print(idf_insert[:-1] + ";", file=idffile)
            print("COMMIT;\nSET autocommit=1;", file=idffile)


def load_disdar_words(text):
    try:
        result = json.loads(text, encoding="UTF-8")
    except json.JSONDecodeError:
        return ""
    try:
        words = result.get("words", [])
    except AttributeError:
        return ""
    return " ".join(words)


def main(instance):
    # TODO: wenn andere instanzen verfügbar (config ist angepasst) rausnehmen:
    if instance != "SEVDESK":
        return

    # config = configparser.ConfigParser()
    #
    # p = os.path.realpath(__file__)
    # confpath = os.path.join(os.path.dirname(p), "config.ini")
    # config.read(confpath)
    config = getconfig()

    sql_clientids = """SELECT DISTINCT
    v.sev_client
FROM
    stitch_sevdesk.voucher v
        JOIN
    stitch_sevdesk.sev_client c 
        ON v.sev_client = c.id_sev_client
WHERE
    v.create > {}
        AND v.status >= 100
        AND v.document IS NOT NULL
        AND c.type NOT IN ('CLOSED')
;"""

    source = config["redshift"]
    sql_user = source["rs_user"]
    sql_pw = source["rs_pw"]
    sql_host = source["rs_host"]
    sql_dbname = source["rs_db"]
    sql_port = int(source["rs_port"])

    # ssh_host = source["ssh_host"]
    # ssh_port = int(source["ssh_port"])
    # ssh_user = source["ssh_user"]

    # sshky = source["ssh_keyfile"]
    # mysshky = paramiko.RSAKey.from_private_key_file(sshky)

    path = config["sql_file"]["sqlfile_path"]

    try:
        os.remove(os.path.join(path, instance+config["sql_file"][
            "sqlfile_words"]))
    except OSError:
        pass
    try:
        os.remove(os.path.join(path, instance+config["sql_file"]["sqlfile_tf"]))
    except OSError:
        pass
    try:
        os.remove(os.path.join(path, instance+config["sql_file"][
            "sqlfile_idf"]))
    except OSError:
        pass

    today = datetime.datetime.today()
    oldest = (today-relativedelta(months=2)).replace(day=1, hour=0, minute=0,
                                                     second=0, microsecond=0)
    oldeststamp = oldest.timestamp()

    # with SSHTunnelForwarder((ssh_host, ssh_port),
    #                         ssh_username=ssh_user,
    #                         ssh_pkey=mysshky,
    #                         remote_bind_address=(sql_host, sql_port)
    #                         ) as tunnel:

    conn = psycopg2.connect(host=sql_host,
                            user=sql_user,
                            password=sql_pw,
                            dbname=sql_dbname,
                            port=sql_port)
    cursor = conn.cursor()
    # print("getting clients")
    cursor.execute(sql_clientids.format(oldeststamp))

    fetchd = cursor.fetchall()

    sev_clientlist = (i[0] for i in fetchd)

    # for id_sevclient in sev_clientlist:
    #     write_to_sqlfile(sev_clientlist, instance, conn, True)
    # print("getting documents for clients")
    df = udv.gather_all_documents(sev_clientlist, conn)
    # print("writing to sqlfile")
    write_df_tosql(df, instance)

    cursor.close()
    conn.close()


if __name__ == '__main__':
    instances = ["SEVDESK", "1AND1", "PEBE"]
    for inst in instances:
        main(inst)
