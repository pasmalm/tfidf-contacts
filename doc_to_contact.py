import numpy as np
import scipy.sparse as sp
from user_docvecs import tokenize_and_stem
from time import time as t
from get_documents import parse_ocr
from docvecs_from_db import restore_vectorization
from sklearn.metrics.pairwise import cosine_distances
from sklearn.feature_extraction.text import CountVectorizer


def main(sev_client, instance, ocr):
    instance = instance.upper()
    vec = restore_vectorization(sev_client, instance)
    if vec:
        dtm, voc, idf, cont = vec
    else:
        return None

    anti_cont = {x: y for y, x in cont.items()}
    words = parse_ocr(ocr)

    doc = [" ".join(words)]

    cv = CountVectorizer(doc, vocabulary=voc, tokenizer=tokenize_and_stem)
    docvec = cv.fit_transform(doc)

    diag = sp.identity(len(idf))  # sparse identity matrix n*n

    tfidf = dtm * diag.multiply(idf)  # jede zeile
    # der docterm matrix komponentenweise multipliziert mit idf

    dists = cosine_distances(tfidf, docvec.multiply(idf))
    mi = np.argmin(dists)
    print("sev_client:", sev_client+instance, "dist minimum: ", dists[mi][0])
    if dists[mi][0] < 0.44:
        return anti_cont[mi]
    else:
        return None


if __name__ == '__main__':
    f = open(r"C:\Users\pm\Documents\lifetest_ocr.json", 'r')
    json = f.read()
    now = t()
    out = main(23, "SEVDESK", json)
    done = t()-now
    print(out, "duration: ", done)
    f = open(r"C:\Users\pm\Documents\lifetest_no_ocr.json", 'r')
    json = f.read()
    now = t()
    out = main(23, "SEVDESK", json)
    done = t()-now
    print(out, "duration: ", done)
    f = open(r"C:\Users\pm\Documents\lifetest_only_ocr.json", 'r')
    json = f.read()
    now = t()
    out = main(23, "SEVDESK", json)
    done = t()-now
    print(out, "duration: ", done)
