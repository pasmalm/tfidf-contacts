import pymysql
import os
from configreader import getconfig
from configparser import ConfigParser as Cp

# pymysql.install_as_MySQLdb()


def truncate_old():
    clean_tf = """truncate table Term_Frequency;"""
    clean_words = """truncate table words;"""
    clean_idf = """truncate table InverseDocumentFrequency;"""

    # config = Cp()
    # p = os.path.realpath(__file__)
    # confpath = os.path.join(os.path.dirname(p), "config.ini")
    # config.read(confpath)

    config = getconfig()
    idf_host = config["tfidf_db"]["tfidf_host"]
    idf_user = config["tfidf_db"]["tfidf_user"]
    idf_pw = config["tfidf_db"]["tfidf_pw"]
    idf_db = config["tfidf_db"]["tfidf_db2"]

    conn = pymysql.connect(host=idf_host,
                           user=idf_user,
                           password=idf_pw,
                           database=idf_db)
    cursor = conn.cursor()

    cursor.execute(clean_idf)
    cursor.execute(clean_tf)
    cursor.execute(clean_words)

    conn.commit()
    cursor.close()
    conn.close()


def switch():
    config = Cp()
    p = os.path.realpath(__file__)
    confpath = os.path.join(os.path.dirname(p), "config.ini")
    config.read(confpath)

    config["tfidf_db"]["tfidf_db"], \
        config["tfidf_db"]["tfidf_db2"] = config["tfidf_db"]["tfidf_db2"], \
        config["tfidf_db"]["tfidf_db"]

    with open(confpath, 'w') as f:
        config.write(f)


if __name__ == '__main__':
    truncate_old()
    switch()
