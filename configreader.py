# -*- coding: utf-8 -*-
import boto3
import configparser


def getconfig():
    config = configparser.ConfigParser()
    s3 = boto3.resource("s3")
    ob = s3.Object("doc-to-contact-artifacts", "config.ini")
    t = ob.get()["Body"].read().decode("utf-8")
    config.read_string(t)
    return config


def main():
    c = getconfig()
    print(list(c.keys()))


if __name__ == '__main__':
    main()
