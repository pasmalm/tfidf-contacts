from doc_to_contact import main


def lambda_handler(event, context):
    if event["ocr"] is None or event["instance"] is None or event[
            "sev_client"] is None:
        return {"contact": None, "request_id": context.aws_request_id}

    cont = main(event["sev_client"], event["instance"], event["ocr"])

    obj = {"contact": cont, "request_id": context.aws_request_id}
    return obj
