import pymysql
import numpy as np
import time as t
from scipy.sparse import dok_matrix
from configreader import getconfig


def restore_vectorization(sev_client, instance):
    """
    restore Vectorization from db  for given sev_client
    :param sev_client: user to restore from db
    :param instance: Wich instance is calling?
    :return: doctermmatrix (document vectors), vocabulary (map words to index
    of doctermmatrix), idf-vector, contacts (map contact id to doctermmatrix
    index)
    """
    # config = configparser.ConfigParser()
    # s3 = boto3.resource("s3")
    # ob = s3.Object("doc-to-contact-artifacts", "config.ini")
    #
    # config.read(ob.get()["Body"].read().decode("utf-8"))
    config = getconfig()

    idf_host = config["tfidf_db"]["tfidf_host"]
    idf_user = config["tfidf_db"]["tfidf_user"]
    idf_pw = config["tfidf_db"]["tfidf_pw"]
    idf_db = config["tfidf_db"]["tfidf_db"]

    conn = pymysql.connect(host=idf_host,
                           user=idf_user,
                           password=idf_pw,
                           database=idf_db)

    sql_check = """SELECT 
    *
FROM
    InverseDocumentFrequency
WHERE
    sev_client = {}
AND instance = '{}'
LIMIT 1
;"""

    sql_voc = """SELECT 
    w.word, idf.index_word
FROM
    InverseDocumentFrequency idf
        JOIN
    words w ON idf.word = w.id_word
WHERE
    sev_client = {}
AND instance = '{}'
;"""

    sql_idf = """SELECT 
    w.word, idf.idf
FROM
    InverseDocumentFrequency idf
        JOIN
    words w ON idf.word = w.id_word
WHERE
    sev_client = {}
AND instance = '{}'
;"""

    sql_contact_tf = """SELECT 
    contact, w.word, count
FROM
    Term_Frequency tf
        JOIN
    words w ON tf.word = w.id_word
WHERE
    sev_client = {}
AND instance = '{}'
;"""

    sql_ncontacts = """SELECT 
    COUNT(DISTINCT contact)
FROM
    Term_Frequency
WHERE
    sev_client = {}
AND instance = '{}'
;"""

    cursor = conn.cursor()

    cursor.execute(sql_check.format(sev_client, instance))
    indb = cursor.fetchall()
    # print(indb)
    # print(len(indb), sev_client)
    if not len(indb):
        return None

    cursor.execute(sql_voc.format(sev_client, instance))

    vocabulary = dict(cursor.fetchall())

    cursor.execute(sql_idf.format(sev_client, instance))

    idfd = dict(cursor.fetchall())
    nwords = max(vocabulary.values())+1
    idf = np.empty(nwords)

    for w in idfd:
        idf[vocabulary[w]] = idfd[w]

    del idfd

    cursor.execute(sql_ncontacts.format(sev_client, instance))
    ncontact = cursor.fetchone()[0]

    doctermm = dok_matrix((ncontact, nwords))
    contacts = {}
    count = 0
    cursor.execute(sql_contact_tf.format(sev_client, instance))
    while True:
        record = cursor.fetchone()
        if record is None:
            break
        if record[0] not in contacts:
            contacts[record[0]] = count
            count += 1
        doctermm[contacts[record[0]], vocabulary[record[1]]] = record[2]

    cursor.close()
    conn.close()
    return doctermm, vocabulary, idf, contacts


if __name__ == '__main__':
    now = t.time()
    a = restore_vectorization(154147, "SEVDESK")
    dur = t.time() - now
    if a:
        print(a[0].shape, len(a[1]), a[2])
        print("duration: ", dur)
    else:
        print("This sev_client is not in DB")
