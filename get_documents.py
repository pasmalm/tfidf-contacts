import json
import boto3


def get_contacts_docs(sev_client, conn):
    """
    Get a list of all 'relevant' contact_ids and their associated document
    for a given sev_client
    :param conn: Connection object to mysqldb
    :param sev_client: int id of sev_client in sevdesk db
    :return: dict of contacts and a list of this contacts documents of this
    sev_client, that have at least a document
    """

    sql = '''SELECT 
    supplier, document
FROM
    voucher
WHERE
    sev_client = {} AND `status` >= 100
        AND document IS NOT NULL
        AND supplier IS NOT NULL
        AND credit_debit = 'c'
;'''

    cursor = conn.cursor()

    cursor.execute(sql.format(sev_client))

    contactdocs = {}
    while True:
        current = cursor.fetchone()
        if current is None:
            break
        if current[0] not in contactdocs:
            contactdocs[current[0]] = []
        contactdocs[current[0]].append(current[1])
    cursor.close()
    return contactdocs


def get_uuid_of_docid(docid, conn):
    """
    for given document id, get the uuid for this doc in disdar labelingtool s3
    bucket
    :param conn: mysql connection to disdar labelingtool db
    :param docid: int document id in sevdesk db voucher table
    :return: str uuid of this document in disdar_labelingtoool s3 bucket
    """

    sql = '''SELECT 
    d.`hash`
FROM
    documents d
        JOIN
    import_logs i ON i.document_id = d.id
WHERE
    i.instanceId = {}
;
    '''

    cursor = conn.cursor()
    cursor.execute(sql.format(docid))
    h = cursor.fetchone()
    if h:
        out = h[0]
    else:
        out = None
    return out


def get_ocrjson(uuid):
    s3 = boto3.resource("s3")
    bucket = s3.Bucket("disdar-labelingtool-storage")
    ocrfile = bucket.Object("documents/"+uuid+"/ocr.json").get()

    return ocrfile["Body"].read()


def parse_ocr(ocrjson):
    """
    extract words from ocr result
    :param ocrjson: str ocr result string
    :return: list<str> all words from the ocr result
    """
    words = []
    try:
        ocrobject = json.loads(ocrjson.decode("utf-16"))
    except json.decoder.JSONDecodeError:
        ocrobject = None
    except AttributeError:  # as ae:
        # print("ATTRIBUTE ERROR", ae)
        try:
            ocrobject = json.loads(ocrjson)
        except json.decoder.JSONDecodeError as jde:
            print("json parsing error !!ERROR!!", jde, ocrjson, sep="\n")
            return ""

    ocrobject = ocrobject.get("objects", ocrobject)

    if "ocr" in ocrobject:
        for w in ocrobject["ocr"]["words"]:
            word = ""
            for char in w:
                word += ocrobject["ocr"]["characters"][char]["value"]
            words.append(word)
    elif "words" in ocrobject:
        if "extractions" in ocrobject:
            words = ocrobject["words"]
        elif "characters" in ocrobject:
            for w in ocrobject["words"]:
                word = ""
                for char in w:
                    word += ocrobject["characters"][char]["value"]
                words.append(word)
    else:
        # print("no ocr or words")
        return None

    return words
