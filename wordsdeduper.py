import sys


def main(filename):
    with open(filename, 'r') as fin:
        lines = {_ for _ in fin.readlines()}

    with open(filename, 'w') as fout:
        lines = "".join(lines)
        print(lines, file=fout)


if __name__ == '__main__':
    try:
        main(sys.argv[1])
    except IndexError:
        print("no filename given")
